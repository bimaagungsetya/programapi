const md5 = require('md5');
const config = require('../config/secret');
const jwt = require('jsonwebtoken');
const UserModel = require('../src/models/user.model');
const TokenModel = require('../src/models/token.model');
const ip = require('ip');


//check user
exports.checkUser = (req, res) => {

    //pass to md5
    passMd5 = md5(req.body.password);

    var userReqData = {
        username: req.body.username,
        password: passMd5
    };

    UserModel.checkUser(userReqData, (err, user) => {
        if (err) {
            res.send(err);
        } else {
            if (user == "") {
                res.json({ status: false, message: 'User verification failed', data: user });
            } else {
                var token = jwt.sign({ user }, config.secret, { expiresIn: '1 days' });
                var idUser = user[0].id_user

                var dataToken = {
                    id_user: idUser,
                    access_token: token,
                    ip_address: ip.address(),
                    created_at: new Date
                }

                TokenModel.createToken(dataToken, (err, tokenAuth) => {
                    if (err) {
                        res.send(err);
                    } else {
                        res.json({ status: true, message: 'Token Has Ben Generate', token: token, current_user: idUser, role_user: user[0].role_user, name_user: user[0].name, username: user[0].username });
                    }
                })
            }
        }
    })

}


//create user
exports.createUser = (req, res) => {

    //pass to md5
    passMd5 = md5(req.body.password);

    var userReqData = {
        name: req.body.name,
        username: req.body.username,
        password: passMd5,
        role_user: 1,
        created_at: new Date(),
        updated_at: new Date()
    };

    if (req.body.contructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({ success: false, message: 'Please fill all field' });
    } else {

        UserModel.createUser(userReqData, (err, user) => {
            if (err) {
                res.send(err);
            } else {
                if (!user.affectedRows) {
                    res.send(user);
                } else {
                    res.json({ status: true, message: 'User Created Successfully', data: user.insertId })
                }
            }
        })
    }
}
