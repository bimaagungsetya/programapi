const jwt = require('jsonwebtoken');
const config = require('../config/secret');

function verification() {
    return function (req, rest, next) {
        //check authorization
        var tokenBearer = req.headers.authorization;
        if (!tokenBearer) {
            return rest.status(401).send({ auth: false, message: 'No Tokens' });
        } else {
            var token = tokenBearer.split(' ')[1];
            //verification
            jwt.verify(token, config.secret, function (err, decoded) {
                if (err) {
                    return rest.status(401).send({ auth: false, message: 'Tokens Not Registered' });
                } else {
                    req.auth = decoded;
                    next();
                }
            });
        }
    }
}

module.exports = verification;