const express = require('express');
const auth = require('./auth');
const router = express.Router();

//create user
router.post('/api/v1/user/register/', auth.createUser);

//check user
router.post('/api/v1/user/', auth.checkUser);

module.exports = router

