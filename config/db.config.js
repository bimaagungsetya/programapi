const mysql = require('mysql');

//create mysql connection

// const dbConn = mysql.createConnection({
//     host: 'localhost',
//     user: 'root',
//     password: '',
//     database: 'db_programapp'
// });

// Connection Mysql Server

const dbConn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'db_programapp'
});

// const dbConn = mysql.createConnection({
//     host: 'localhost',
//     user: 'otetlabs_root',
//     password: 'Macbookpro100#@',
//     database: 'otetlabs_db_programapp'
// });

dbConn.connect(function (error) {
    if (error) throw error;
    console.log('Database Connected Successfully');
});

module.exports = dbConn;
