const base_url = require('../../config/base_url');
const PhotoMainModel = require('../models/photo_main.model');

//get all photo main
exports.getMainPhoto = (req, res) => {

    PhotoMainModel.getMainPhoto((err, photos) => {
        if (err) {
            res.send(err);
        } else {
            if (photos == "") {
                res.json({ status: false, message: 'Empty Main Photo Data', data: photos })
            } else {

                var dataUser = []
                for (let i = 0; i < photos.length; ++i) {
                    dataUser.push({
                        id_photos_main: photos[i].id_photos_main,
                        photo: base_url.baseUrlServer + 'main_photo/' + photos[i].photo,
                        created_at: photos[i].created_at,
                        upated_at: photos[i].upated_at
                    });
                }

                res.json({ status: true, message: 'Get Main Photo Successfully', data: photos })
            }
        }
    })

}