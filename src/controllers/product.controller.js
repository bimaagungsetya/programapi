const base_url = require('../../config/base_url');
const ProductModel = require('../models/product.model');

// get new product limit 5
exports.getNewProduct = (req, res) => {

    ProductModel.getNewProduct((err, product) => {
        if (err) {
            res.send(err);
        } else {
            if (product == "") {
                res.json({ status: false, message: 'Empty New Product Data', data: product })
            } else {
                var listProduct = [];

                for (let i = 0; i < product.length; ++i) {
                    var resultVideo = product[i].video;

                    var video_url = '';
                    if (resultVideo != 'Undefined') {
                        video_url = base_url.baseUrlServer + 'video/' + product[i].video;
                    } else {
                        video_url = 'Undefined';
                    }
                    listProduct.push({
                        id_product: product[i].id_product,
                        title: product[i].title,
                        sub_title: product[i].sub_title,
                        description: product[i].description,
                        cover: base_url.baseUrlServer + 'cover_content/' + product[i].cover,
                        video: video_url,
                        contact: product[i].contact,
                        count_like: product[i].count_like,
                        new_content: product[i].new_content,
                        created_at: new Date(),
                        updated_at: new Date()
                    });
                }

                res.json({ status: true, message: 'Get New Product Successfully', data: listProduct })
            }
        }
    })
}

//get all product
exports.getAllProduct = (req, res) => {

    ProductModel.getAllProduct((err, product) => {
        if (err) {

            res.send(err);

        } else {
            if (product == "") {

                res.json({ status: false, message: 'Empty Product Data', data: product })

            } else {
                var listProduct = [];

                for (let i = 0; i < product.length; ++i) {
                    var resultVideo = product[i].video;

                    var video_url = '';
                    if (resultVideo != 'Undefined') {
                        video_url = base_url.baseUrlServer + 'video/' + product[i].video;
                    } else {
                        video_url = 'Undefined';
                    }
                    listProduct.push({
                        id_product: product[i].id_product,
                        title: product[i].title,
                        sub_title: product[i].sub_title,
                        description: product[i].description,
                        cover: base_url.baseUrlServer + 'cover_content/' + product[i].cover,
                        video: video_url,
                        contact: product[i].contact,
                        count_like: product[i].count_like,
                        new_content: product[i].new_content,
                        created_at: new Date(),
                        updated_at: new Date()
                    });
                };

                res.json({ status: true, message: 'Get Product Successfully', data: listProduct });
            }
        }
    })
}

//create new product
exports.createProduct = (req, res) => {

    //upload file
    var datetimestamp = Date.now();
    const fileCover = req.files.cover;
    var nameCover = fileCover.name.split('.')[0] + '_' + datetimestamp + '.' + fileCover.name.split('.')[fileCover.name.split('.').length - 1];


    const fileVideo = req.files.cover;

    if (fileVideo != null) {
        var nameVideo = fileVideo.name.split('.')[0] + '_' + datetimestamp + '.' + fileVideo.name.split('.')[fileVideo.name.split('.').length - 1];
    } else {
        var nameVideo = "Undefined";
    }

    var productReqData = {
        title: req.body.title,
        sub_title: req.body.sub_title,
        description: req.body.description,
        cover: nameCover,
        video: nameVideo,
        contact: req.body.contact,
        count_like: req.body.count_like,
        new_content: 1,
        created_at: new Date(),
        updated_at: new Date()
    };

    // check null
    if (req.body.contructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({ success: false, message: 'Please fill all field' });
    } else {
        if (fileVideo != null) {
            fileVideo.mv('uploads/video/' + nameVideo, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    fileCover.mv('uploads/cover_content/' + nameCover, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            ProductModel.createProduct(productReqData, (err, product) => {
                                if (err) {
                                    res.send(err);
                                } else {
                                    if (!product.affectedRows) {
                                        res.send(product);
                                    } else {
                                        res.json({ status: true, message: 'Product Created Successfully', data: product.insertId })
                                    }
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fileCover.mv('uploads/cover_content/' + nameCover, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    ProductModel.createProduct(productReqData, (err, product) => {
                        if (err) {
                            res.send(err);
                        } else {
                            if (!product.affectedRows) {
                                res.send(product);
                            } else {
                                res.json({ status: true, message: 'Product Created Successfully', data: product.insertId })
                            }
                        }
                    });
                }
            });
        }
    }
}