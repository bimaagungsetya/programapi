const OutletModel = require('../models/outlet.model');

//get outlet by id outlet
exports.getOutletByIdOutlet = (req, res) => {

    var id_outlet = req.params.id_outlet;

    OutletModel.getOutletByIdOutlet(id_outlet, (err, outlet) => {
        if (err) {
            res.send(err);
        } else {
            if (outlet == "") {
                res.json({ status: false, message: 'Empty Outlet Data', data: outlet })
            } else {
                console.log('Outlet', outlet);
                res.json({ status: true, message: 'Get Outlet By Id Outlet Successfully', data: outlet })
            }
        }
    })
}