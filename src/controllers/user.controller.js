
const md5 = require('md5');
const base_url = require('../../config/base_url');
const UserModel = require('../models/user.model');



//get all user
exports.getUserList = (req, res) => {
    UserModel.getAllUser((err, users) => {
        if (err) {
            res.send(err);
        } else {
            if (users == "") {
                res.json({ status: false, message: 'Empty User Data', data: users })
            } else {
                var dataUser = []
                for (let i = 0; i < users.length; ++i) {
                    dataUser.push({
                        id_user: users[i].id_user,
                        name: users[i].name,
                        username: users[i].username,
                        //avatar: base_url.baseUrlServer + 'avatar/' + users[i].avatar,
                        role_user: users[i].role_user,
                        created_at: users[i].created_at,
                        upated_at: users[i].upated_at
                    });
                }
                console.log('User', dataUser);
                res.json({ status: true, message: 'Get All User Successfully', data: dataUser })
            }
        }
    })
}

//get user by id
exports.getUserByID = (req, res) => {

    var id = req.params.id;
    UserModel.getUserByID(id, (err, user) => {
        if (err) {
            res.send(err);
        } else {
            if (user == "") {
                res.json({ status: false, message: 'Empty User Data', data: user })
            } else {
                var dataUser = [{
                    id_user: user[0].id_user,
                    name: user[0].name,
                    username: user[0].username,
                    //avatar: base_url.baseUrlServer + 'avatar/' + user[0].avatar,
                    role_user: user[0].role_user,
                    created_at: user[0].created_at,
                    upated_at: user[0].upated_at
                }];
                console.log('User', user);
                res.json({ status: true, message: 'Get User By Id Successfully', data: dataUser })
            }
        }
    })
}

//create user
exports.changePass = (req, res) => {

    //pass to md5
    passMd5 = md5(req.body.password);

    var userReqData = {
        id_user: req.body.id_user,
        password: passMd5,
        updated_at: new Date()
    };

    if (req.body.contructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({ success: false, message: 'Please fill all field' });
    } else {

        UserModel.changePass(userReqData, (err, user) => {
            if (err) {
                res.send(err);
            } else {
                if (!user.affectedRows) {
                    res.send(user);
                } else {
                    res.json({ status: true, message: 'User Change Password Successfully', data: user.insertId })
                }
            }
        })
    }
}



