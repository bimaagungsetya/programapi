const VideoModel = require('../models/video.model');
const base_url = require('../../config/base_url');

//get new video main
exports.getNewVideo = (req, res) => {

    VideoModel.getNewVideo((err, videos) => {
        if (err) {
            res.send(err);
        } else {
            if (videos == "") {
                res.json({ status: false, message: 'Empty New Video Data', data: videos })
            } else {
                var listVideo = [];

                for (let i = 0; i < videos.length; ++i) {
                    listVideo.push({
                        id_video: videos[i].id_video,
                        title: videos[i].title,
                        cover: base_url.baseUrlServer + 'cover_content/' + videos[i].cover,
                        video: base_url.baseUrlServer + 'video/' + videos[i].video,
                        detail: videos[i].detail,
                        periode: videos[i].periode,
                        new_content: videos[i].new_content,
                        created_at: new Date(),
                        updated_at: new Date()
                    });
                }

                res.json({ status: true, message: 'Get New Video Successfully', data: listVideo })
            }
        }
    })

}

//get all video main
exports.getAllVideo = (req, res) => {

    VideoModel.getAllVideo((err, videos) => {
        if (err) {
            res.send(err);
        } else {
            if (videos == "") {
                res.json({ status: false, message: 'Empty All Video Data', data: videos })
            } else {
                var listVideo = [];

                for (let i = 0; i < videos.length; ++i) {
                    listVideo.push({
                        id_video: videos[i].id_video,
                        title: videos[i].title,
                        cover: base_url.baseUrlServer + 'cover_content/' + videos[i].cover,
                        video: base_url.baseUrlServer + 'video/' + videos[i].video,
                        detail: videos[i].detail,
                        periode: videos[i].periode,
                        new_content: videos[i].new_content,
                        created_at: new Date(),
                        updated_at: new Date()
                    });
                }

                res.json({ status: true, message: 'Get All Video Successfully', data: listVideo })
            }
        }
    })

}

//create new video
exports.createVideo = (req, res) => {

    //upload cover
    var datetimestamp = Date.now();
    const fileCover = req.files.cover;
    var nameCover = fileCover.name.split('.')[0] + '_' + datetimestamp + '.' + fileCover.name.split('.')[fileCover.name.split('.').length - 1];

    //upload video
    const fileVideo = req.files.video;
    var nameVideo = fileVideo.name.split('.')[0] + '_' + datetimestamp + '.' + fileVideo.name.split('.')[fileVideo.name.split('.').length - 1];


    var videoReqData = {
        title: req.body.title,
        cover: nameCover,
        video: nameVideo,
        detail: req.body.detail,
        periode: req.body.periode,
        new_content: 0,
        created_at: new Date(),
        updated_at: new Date()
    };

    // check null
    if (req.body.contructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({ success: false, message: 'Please fill all field' });
    } else {
        fileCover.mv('uploads/cover_content/' + nameCover, (err) => {

            if (err) {
                console.log(err);

            } else {

                fileVideo.mv('uploads/video/' + nameVideo, (err) => {
                    if (err) {
                        console.log(err);
                    } else {

                        VideoModel.createVideo(videoReqData, (err, videos) => {
                            if (err) {
                                res.send(err);
                            } else {
                                if (!videos.affectedRows) {
                                    res.send(videos);
                                } else {
                                    res.json({ status: true, message: 'Video Created Successfully', data: videos.insertId })
                                }
                            }
                        });


                    }
                });


            }
        });


    }
}