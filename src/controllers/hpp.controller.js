const HppModel = require('../models/hpp.model');

//get calculator hpp
exports.getHpp = (req, res) => {

    var cb = req.body.cb;
    var topProd = req.body.top_prod;
    var tng = req.body.tng;
    var jawara = req.body.jawara;
    var sultan = req.body.sultan;
    var txtJawara = req.body.txt_jawara;
    var txtSultan = req.body.txt_sultan;

    console.log(
        cb+" "+topProd+" "+tng+" "+jawara+" "+sultan+" "+txtJawara+" "+txtSultan
    );


    HppModel.getDataHpp((err, result) => {
        if (err) {

            res.send(err);

        } else {
            if (result == "") {

                res.json({ status: false, message: 'Empty Hpp Data', data: result })

            } else {
                var listHpp = [];

                for (let i = 0; i < result.length; ++i) {
                    
                    var harga_new_sp = result[i].harga_new_sp;
                    var harga_rebuy = result[i].harga_rebuy;
                    var harga_vc = result[i].harga_vc;

                    var harga_new_sp_fix;
                    var harga_rebuy_fix;
                    var harga_vc_fix;

                    var itfJawara;
                    var itfSultan;

                    //Segment Program

                    console.log("input"+jawara);
                    console.log("db"+result[i].jawara);

                    if(jawara == result[i].jawara){

                        if(txtJawara == 1){

                            itfJawara = 0.04; // 100 rb

                        }else if(txtJawara == 2){

                            itfJawara = 0.06; // 1 jt 

                        }else if(txtJawara == 3){

                            itfJawara = 0.08; // 10 jt 

                        }else if(txtJawara == 4){

                            itfJawara = 0.1; // 100 jt

                        }else{
                            itfJawara = 0.0; // no detect
                            console.log("jawara no detect")
                        }

                    }else if(sultan == result[i].sultan){

                        if(txtSultan == 1){

                            itfSultan = 0.04; //ONO

                        }else if(txtSultan == 2){

                            itfSultan = 0.035; // < 125 Jt

                        }else if(txtSultan == 3){

                            itfSultan = 0.036; // 125 Jt

                        }else if(txtSultan == 4){

                            itfSultan = 0.038; // 1 M

                        }else if(txtSultan == 4){

                            itfSultan = 0.04; // 4 M

                        }else{

                            itfSultan = 0.0; // no detect
                            console.log("sultan no detect")

                        }

                    }else{
                        itfJawara = 0.0;
                        itfSultan = 0.0;
                        console.log("Program No Detect");
                    }

                    //Calculation

                    if(cb == 1 && topProd == 0 && tng == 0 && jawara == 0 && sultan == 0){ //1

                        harga_new_sp_fix = harga_new_sp - result[i].cb_new_sp;
                        harga_rebuy_fix = harga_rebuy - result[i].cb_rebuy;
                        harga_vc_fix = harga_vc - result[i].cb_vc;


                    }else if(cb == 0 && topProd == 1 && tng == 0 && jawara == 0 && sultan == 0){ // 1

                        harga_new_sp_fix = harga_new_sp - result[i].top_prod_new_sp;
                        harga_rebuy_fix = harga_rebuy - result[i].top_prod_rebuy;
                        harga_vc_fix = harga_vc - result[i].top_prod_vc;

                    }else if(cb ==01 && topProd == 0 && tng == 1 && jawara == 0 && sultan == 0){  // 1

                        harga_new_sp_fix = harga_new_sp - result[i].ctg_new_sp;
                        harga_rebuy_fix = harga_rebuy - result[i].ctg_rebuy;
                        harga_vc_fix = harga_vc - result[i].ctg_vc;

                    }else if(cb == 0 && topProd == 0 && tng == 0 && jawara == 1 && sultan == 0){ // 1 

                        harga_new_sp_fix = Math.floor(harga_new_sp - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor(harga_rebuy - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor(harga_vc - (result[i].harga_mobo*itfJawara));

                    }else if(cb == 0 && topProd == 0 && tng == 0 && jawara == 0 && sultan == 1){  // 1

                        harga_new_sp_fix = Math.floor(harga_new_sp - (result[i].harga_mobo*itfSultan));
                        harga_rebuy_fix = Math.floor(harga_rebuy - (result[i].harga_mobo*itfSultan));
                        harga_vc_fix = Math.floor(harga_vc - (result[i].harga_mobo*itfSultan));

                    }else if(cb == 1 && topProd == 1 && tng == 0 && jawara == 0 && sultan == 0){ // 2

                        harga_new_sp_fix = (harga_new_sp - result[i].cb_new_sp) - result[i].top_prod_new_sp;
                        harga_rebuy_fix = (harga_rebuy - result[i].cb_rebuy) - result[i].top_prod_rebuy;
                        harga_vc_fix =(harga_vc - result[i].cb_vc) - result[i].top_prod_vc;

                    }else if(cb == 1 && topProd == 0 && tng == 1 && jawara == 0 && sultan == 0){ // 2

                        harga_new_sp_fix = (harga_new_sp - result[i].cb_new_sp) - result[i].ctg_new_sp;
                        harga_rebuy_fix = (harga_rebuy - result[i].cb_rebuy) - result[i].ctg_rebuy;
                        harga_vc_fix = (harga_vc - result[i].cb_vc) - result[i].ctg_vc;

                    }else if(cb == 1 && topProd == 0 && tng == 0 && jawara == 1 && sultan == 0){ // 2

                        harga_new_sp_fix = Math.floor((harga_new_sp - result[i].cb_new_sp) - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor((harga_rebuy - result[i].cb_rebuy) - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor((harga_vc - result[i].cb_vc) - (result[i].harga_mobo*itfJawara));
                       
                    }else if(cb == 1 && topProd == 0 && tng == 0 && jawara == 0 && sultan == 1){ // 2

                        harga_new_sp_fix = Math.floor((harga_new_sp - result[i].cb_new_sp) - (result[i].harga_mobo*itfSultan));
                        harga_rebuy_fix = Math.floor((harga_rebuy - result[i].cb_rebuy) - (result[i].harga_mobo*itfSultan));
                        harga_vc_fix = Math.floor((harga_vc - result[i].cb_vc) - (result[i].harga_mobo*itfSultan));

                    }else if(cb == 0 && topProd == 1 && tng == 1 && jawara == 0 && sultan == 0){ // 2

                        harga_new_sp_fix = (harga_new_sp - result[i].top_prod_new_sp) - result[i].ctg_new_sp;
                        harga_rebuy_fix = (harga_rebuy - result[i].top_prod_rebuy) - result[i].ctg_rebuy;
                        harga_vc_fix = (harga_vc - result[i].top_prod_vc) - result[i].ctg_vc;

                    }else if(cb == 0 && topProd == 1 && tng == 0 && jawara == 1 && sultan == 0){ // 2

                        harga_new_sp_fix = Math.floor((harga_new_sp - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor((harga_rebuy - result[i].top_prod_rebuy) - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor((harga_vc - result[i].top_prod_vc) - (result[i].harga_mobo*itfJawara));

                    }else if(cb == 0 && topProd == 1 && tng == 0 && jawara == 0 && sultan == 1){ // 2

                        harga_new_sp_fix = Math.floor((harga_new_sp - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfSultan));
                        harga_rebuy_fix = Math.floor((harga_rebuy - result[i].top_prod_rebuy) - (result[i].harga_mobo*itfSultan));
                        harga_vc_fix = Math.floor((harga_vc - result[i].top_prod_vc) - (result[i].harga_mobo*itfSultan));

                    }else if(cb == 0 && topProd == 0 && tng == 1 && jawara == 1 && sultan == 0){ // 2

                        harga_new_sp_fix = Math.floor((harga_new_sp - result[i].ctg_new_sp) - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor((harga_rebuy - result[i].ctg_rebuy) - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor((harga_vc - result[i].ctg_vc) - (result[i].harga_mobo*itfJawara));
                      
                    }else if(cb == 0 && topProd == 0 && tng == 1 && jawara == 0 && sultan == 1){ // 2

                        harga_new_sp_fix = Math.floor((harga_new_sp - result[i].ctg_new_sp) - (result[i].harga_mobo*itfSultan));
                        harga_rebuy_fix = Math.floor((harga_rebuy - result[i].ctg_rebuy) - (result[i].harga_mobo*itfSultan));
                        harga_vc_fix = Math.floor((harga_vc - result[i].ctg_vc) - (result[i].harga_mobo*itfSultan));

                    }else if(cb == 0 && topProd == 0 && tng == 0 && jawara == 1 && sultan == 1){ // 2

                        harga_new_sp_fix = Math.floor(Math.floor(harga_new_sp - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor(Math.floor(harga_rebuy - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor(Math.floor(harga_vc - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));

                    }else if(cb == 1 && topProd == 1 && tng == 1 && jawara == 0 && sultan == 0){ // 3

                        harga_new_sp_fix = ((harga_new_sp - result[i].cb_new_sp) - result[i].top_prod_new_sp) - result[i].ctg_new_sp;
                        harga_rebuy_fix = ((harga_rebuy - result[i].cb_rebuy) - result[i].top_prod_rebuy) - result[i].ctg_rebuy;
                        harga_vc_fix = ((harga_vc - result[i].cb_vc) - result[i].top_prod_vc) - result[i].ctg_vc;

                    }else if(cb == 1 && topProd == 0 && tng == 1 && jawara == 1 && sultan == 0){ // 3

                        harga_new_sp_fix = ((harga_new_sp - result[i].cb_new_sp) - result[i].top_prod_new_sp) - result[i].ctg_new_sp;
                        harga_rebuy_fix = ((harga_rebuy - result[i].cb_rebuy) - result[i].top_prod_rebuy) - result[i].ctg_rebuy;
                        harga_vc_fix = ((harga_vc - result[i].cb_vc) - result[i].top_prod_vc) - result[i].ctg_vc;

                    }else if(cb == 1 && topProd == 0 && tng == 0 && jawara == 1 && sultan == 1){ // 3

                        harga_new_sp_fix = Math.floor(Math.floor((harga_new_sp - result[i].cb_new_sp) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));
                        harga_rebuy_fix = Math.floor(Math.floor((harga_rebuy - result[i].cb_rebuy) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));
                        harga_vc_fix = Math.floor(Math.floor((harga_vc - result[i].cb_vc) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));

                    }else if(cb == 0 && topProd == 1 && tng == 1 && jawara == 1 && sultan == 0){ // 3

                        harga_new_sp_fix = Math.floor(((harga_new_sp - result[i].top_prod_new_sp) - result[i].ctg_new_sp) - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor(((harga_rebuy - result[i].top_prod_rebuy) - result[i].ctg_rebuy) - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor(((harga_vc - result[i].top_prod_vc) - result[i].ctg_vc) - (result[i].harga_mobo*itfJawara));

                    }else if(cb == 0 && topProd == 1 && tng == 0 && jawara == 1 && sultan == 1){ // 3

                        harga_new_sp_fix = Math.floor(Math.floor((harga_new_sp - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));
                        harga_rebuy_fix = Math.floor(Math.floor((harga_rebuy - result[i].top_prod_rebuy) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));
                        harga_vc_fix = Math.floor(Math.floor((harga_vc - result[i].top_prod_vc) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));

                    }else if(cb == 0 && topProd == 0 && tng == 1 && jawara == 1 && sultan == 1){ // 3

                        harga_new_sp_fix = Math.floor(Math.floor((harga_new_sp - result[i].ctg_new_sp) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));
                        harga_rebuy_fix = Math.floor(Math.floor((harga_rebuy - result[i].ctg_rebuy) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));
                        harga_vc_fix = Math.floor(Math.floor((harga_vc - result[i].ctg_vc) - (result[i].harga_mobo*itfJawara)) - (result[i].harga_mobo*itfSultan));

                    }else if(cb == 1 && topProd == 1 && tng == 1 && jawara == 1 && sultan == 0){ // 4

                        harga_new_sp_fix = Math.floor((((harga_new_sp - result[i].cb_new_sp) - result[i].ctg_new_sp) - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor((((harga_rebuy - result[i].cb_rebuy) - result[i].ctg_rebuy) - result[i].top_prod_rebuy) - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor((((harga_vc - result[i].cb_vc) - result[i].ctg_vc) - result[i].top_prod_vc) - (result[i].harga_mobo*itfJawara));

                    }else if(cb == 1 && topProd == 1 && tng == 1 && jawara == 0 && sultan == 1){ // 4

                        harga_new_sp_fix = Math.floor((((harga_new_sp - result[i].cb_new_sp) - result[i].ctg_new_sp) - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfSultan));
                        harga_rebuy_fix = Math.floor((((harga_rebuy - result[i].cb_rebuy) - result[i].ctg_rebuy) - result[i].top_prod_rebuy) - (result[i].harga_mobo*itfSultan));
                        harga_vc_fix = Math.floor((((harga_vc - result[i].cb_vc) - result[i].ctg_vc) - result[i].top_prod_vc) - (result[i].harga_mobo*itfSultan));

                    }else if(cb == 0 && topProd == 1 && tng == 1 && jawara == 1 && sultan == 1){ // 4

                        harga_new_sp_fix = Math.floor(Math.floor(((harga_new_sp - result[i].ctg_new_sp) - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor(Math.floor(((harga_rebuy - result[i].ctg_rebuy) - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor(Math.floor(((harga_vc - result[i].ctg_vc) - result[i].top_prod_vc) - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));

                    }else if(cb == 1 && topProd == 1 && tng == 1 && jawara == 1 && sultan == 1){ // 5

                        harga_new_sp_fix = Math.floor(Math.floor((((harga_new_sp - result[i].cb_new_sp) - result[i].ctg_new_sp) - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));
                        harga_rebuy_fix = Math.floor(Math.floor((((harga_new_sp - result[i].cb_rebuy) - result[i].ctg_rebuy) - result[i].top_prod_new_sp) - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));
                        harga_vc_fix = Math.floor(Math.floor((((harga_new_sp - result[i].cb_vc) - result[i].ctg_vc) - result[i].top_prod_vc) - (result[i].harga_mobo*itfSultan)) - (result[i].harga_mobo*itfJawara));
                       
                    }else{
                        console.log("No Detect");
                    }

                    listHpp.push({
                        id_hpp: result[i].id_hpp,
                        kategori: result[i].kategori,
                        product: result[i].product,
                        harga_mobo: result[i].harga_mobo,
                        denom: result[i].denom,
                        harga_hpp_new_sp : harga_new_sp_fix,
                        harga_hpp_rebuy : harga_rebuy_fix,
                        harga_hpp_vc : harga_vc_fix
                    });
                   
                };

                res.json({ status: true, message: 'Get Calculator Hpp Successfully', data: listHpp });

            }
        }
    })
}