const JawaraModel = require('../models/jawara.model');
const JawaraSPModel = require('../models/jawara_sp.model');
const SultanModel = require('../models/sultan.model');

//get jawara by id outlet
exports.getJawaraByIdOutlet = (req, res) => {

    var id_outlet = req.params.id_outlet;

    JawaraModel.getJawaraByIdOutlet(id_outlet, (err, jawara) => {
        if (err) {
            res.send(err);
        } else {
            if (jawara == "") {
                res.json({ status: false, message: 'Empty Jawara Data', data: jawara })
            } else {
                res.json({ status: true, message: 'Get Jawara By Id Outlet Successfully', data: jawara })
            }
        }
    })
}

//get jawara sp by id outlet
exports.getJawaraSPByIdOutlet = (req, res) => {

    var id_outlet = req.params.id_outlet;

    JawaraSPModel.getJawaraSPByIdOutlet(id_outlet, (err, jawaraSP) => {
        if (err) {
            res.send(err);
        } else {
            if (jawaraSP == "") {
                res.json({ status: false, message: 'Empty Jawara SP Data', data: jawaraSP })
            } else {
                res.json({ status: true, message: 'Get Jawara SP By Id Outlet Successfully', data: jawaraSP })
            }
        }
    })
}

//get sultan by id outlet
exports.getSultanByIdOutlet = (req, res) => {

    var id_outlet = req.params.id_outlet;

    SultanModel.getSultanByIdOutlet(id_outlet, (err, sultan) => {
        if (err) {
            res.send(err);
        } else {
            if (sultan == "") {
                res.json({ status: false, message: 'Empty Sultan Data', data: sultan })
            } else {
                res.json({ status: true, message: 'Get Sultan By Id Outlet Successfully', data: sultan })
            }
        }
    })
}