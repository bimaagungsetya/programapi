const base_url = require('../../config/base_url');
const ProgramModel = require('../models/program.model');

// get new program limit 5
exports.getNewProgram = (req, res) => {

    ProgramModel.getNewProgram((err, program) => {
        if (err) {
            res.send(err);
        } else {
            if (program == "") {
                res.json({ status: false, message: 'Empty New Program Data', data: program })
            } else {
                var listProgram = [];

                for (let i = 0; i < program.length; ++i) {
                    var resultVideo = program[i].video;
                    var video_url = '';
                    if (resultVideo != 'Undefined') {
                        video_url = base_url.baseUrlServer + 'video/' + program[i].video;
                    } else {
                        video_url = 'Undefined';
                    }
                    listProgram.push({
                        id_program: program[i].id_program,
                        title: program[i].title,
                        sub_title: program[i].sub_title,
                        description: program[i].description,
                        cover: base_url.baseUrlServer + 'cover_content/' + program[i].cover,
                        video: video_url,
                        contact: program[i].contact,
                        count_like: program[i].count_like,
                        new_content: program[i].new_content,
                        created_at: new Date(),
                        updated_at: new Date()
                    });
                }

                res.json({ status: true, message: 'Get New Program Successfully', data: listProgram })
            }
        }
    })
}

//get all program
exports.getAllProgram = (req, res) => {

    ProgramModel.getAllProgram((err, program) => {
        if (err) {

            res.send(err);

        } else {
            if (program == "") {

                res.json({ status: false, message: 'Empty Program Data', data: program })

            } else {
                var listProgram = [];

                for (let i = 0; i < program.length; ++i) {
                    var resultVideo = program[i].video;

                    var video_url = '';
                    if (resultVideo != 'Undefined') {
                        video_url = base_url.baseUrlServer + 'video/' + program[i].video;
                    } else {
                        video_url = 'Undefined';
                    }
                    listProgram.push({
                        id_program: program[i].id_program,
                        title: program[i].title,
                        sub_title: program[i].sub_title,
                        description: program[i].description,
                        cover: base_url.baseUrlServer + 'cover_content/' + program[i].cover,
                        video: video_url,
                        contact: program[i].contact,
                        count_like: program[i].count_like,
                        new_content: program[i].new_content,
                        created_at: new Date(),
                        updated_at: new Date()
                    });
                };

                res.json({ status: true, message: 'Get Program Successfully', data: listProgram });
            }
        }
    })
}

//create new program
exports.createProgram = (req, res) => {

    //upload file
    var datetimestamp = Date.now();

    const fileCover = req.files.cover;
    var nameCover = fileCover.name.split('.')[0] + '_' + datetimestamp + '.' + fileCover.name.split('.')[fileCover.name.split('.').length - 1];

    const fileVideo = req.files.video;
    if (fileVideo == null) {
        var nameVideo = "Undefined";
    } else {
        var nameVideo = fileVideo.name.split('.')[0] + '_' + datetimestamp + '.' + fileVideo.name.split('.')[fileVideo.name.split('.').length - 1];
    }




    var programReqData = {
        title: req.body.title,
        sub_title: req.body.sub_title,
        description: req.body.description,
        cover: nameCover,
        video: nameVideo,
        contact: req.body.contact,
        count_like: req.body.count_like,
        new_content: 1,
        created_at: new Date(),
        updated_at: new Date()
    };

    // check null
    if (req.body.contructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({ success: false, message: 'Please fill all field' });
    } else {
        if (fileVideo != null) {
            fileVideo.mv('uploads/video/' + nameVideo, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    fileCover.mv('uploads/cover_content/' + nameCover, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            ProgramModel.createProgram(programReqData, (err, program) => {
                                if (err) {
                                    res.send(err);
                                } else {
                                    if (!program.affectedRows) {
                                        res.send(program);
                                    } else {
                                        res.json({ status: true, message: 'Program Created Successfully', data: program.insertId })
                                    }
                                }
                            });

                        }
                    });
                }
            });
        } else {
            fileCover.mv('uploads/cover_content/' + nameCover, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    ProgramModel.createProgram(programReqData, (err, program) => {
                        if (err) {
                            res.send(err);
                        } else {
                            if (!program.affectedRows) {
                                res.send(program);
                            } else {
                                res.json({ status: true, message: 'Program Created Successfully', data: program.insertId })
                            }
                        }
                    });

                }
            });

        }

    }
}