const ContactUsModel = require('../models/contact_us.model');

//send message contact us
exports.sendMessage = (req, res) => {

    const messageReqData = new ContactUsModel(req.body);
    console.log('messageReqData', messageReqData);

    // check null
    if (req.body.contructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({ success: false, message: 'Please fill all field' });
    } else {
        ContactUsModel.sendMessage(messageReqData, (err, contactUs) => {
            if (err) {
                res.send(err);
            } else {
                if (!contactUs.affectedRows) {
                    res.send(contactUs);
                } else {
                    res.json({ status: true, message: 'Send Message Successfully', data: contactUs.insertId })
                }
            }
        })
    }
}