const base_url = require('../../config/base_url');
const ContentModel = require('../models/content.model');
const PhotoDetailModel = require('../models/photo_detail.model');

// get new content by category limit 5
exports.getNewContentCategory = (req, res) => {

    var category = req.params.category;
    ContentModel.getNewContentCategory(category, (err, content) => {
        if (err) {
            res.send(err);
        } else {
            if (content == "") {
                res.json({ status: false, message: 'Empty New Content Data', data: content })
            } else {
                var listContent = [];

                for (let i = 0; i < content.length; ++i) {
                    listContent.push({
                        id_detail: content[i].id_detail,
                        title: content[i].title,
                        periode: content[i].periode,
                        category: content[i].category,
                        ketentuan: content[i].ketentuan,
                        loyalti: content[i].loyalti,
                        info: content[i].info,
                        cover: base_url.baseUrlServer + 'cover_content/' + content[i].cover,
                        new_content: content[i].new_content,
                        created_at: new Date(),
                        updated_at: new Date()
                    });
                }
                res.json({ status: true, message: 'Get New Content By Category Successfully', data: listContent })
            }
        }
    })
}


//get content by category
exports.getContentCategory = (req, res) => {

    var category = req.params.category;
    ContentModel.getContentCategory(category, (err, content) => {
        if (err) {
            res.send(err);
        } else {
            if (content == "") {
                res.json({ status: false, message: 'Empty Content Data', data: content })
            } else {
                var listContent = [];

                for (let i = 0; i < content.length; ++i) {
                    listContent.push({
                        id_detail: content[i].id_detail,
                        title: content[i].title,
                        periode: content[i].periode,
                        category: content[i].category,
                        ketentuan: content[i].ketentuan,
                        loyalti: content[i].loyalti,
                        info: content[i].info,
                        cover: base_url.baseUrlServer + 'cover_content/' + content[i].cover,
                        new_content: content[i].new_content,
                        created_at: new Date(),
                        updated_at: new Date()
                    });
                }

                res.json({ status: true, message: 'Get Content By Category Successfully', data: listContent })
            }
        }
    })
}

//get photo by id detail
exports.getPhotoByIdDetail = (req, res) => {

    var reqDataPhoto = {
        id_detail: req.body.id_detail,
        category: req.body.category,
        position: req.body.position
    }
    PhotoDetailModel.getPhotoByIdDetail(reqDataPhoto, (err, photo) => {
        if (err) {
            res.send(err);
        } else {
            if (photo == "") {
                res.json({ status: false, message: 'Empty Detail Photo Data', data: photo })
            } else {
                var photos = [];

                for (let i = 0; i < photo.length; ++i) {
                    photos.push({
                        id_detail_photo: photo[i].id_detail_photo,
                        id_detail: photo[i].id_detail,
                        category: photo[i].category,
                        photo: base_url.baseUrlServer + 'detail_photo/' + photo[i].photo,
                        position: photo[i].position,
                        created_at: photo[i].created_at,
                        updated_at: photo[i].updated_at,
                    });
                }

                res.json({ status: true, message: 'Get Photo Detail By Id Detail Successfully', data: photos })
            }
        }
    })
}

//create new content
exports.createContent = (req, res) => {

    //upload file
    var datetimestamp = Date.now();
    const file = req.files.cover;
    var nameCover = file.name.split('.')[0] + '_' + datetimestamp + '.' + file.name.split('.')[file.name.split('.').length - 1];


    var contentReqData = {
        title: req.body.title,
        periode: req.body.periode,
        category: req.body.category,
        ketentuan: req.body.ketentuan,
        loyalti: req.body.loyalti,
        info: req.body.info,
        cover: nameCover,
        new_content: 0,
        created_at: new Date(),
        updated_at: new Date()
    };

    // check null
    if (req.body.contructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({ success: false, message: 'Please fill all field' });
    } else {
        file.mv('uploads/cover_content/' + nameCover, (err) => {
            if (err) {
                console.log(err);
            } else {
                ContentModel.createContent(contentReqData, (err, content) => {
                    if (err) {
                        res.send(err);
                    } else {
                        if (!content.affectedRows) {
                            res.send(content);
                        } else {
                            res.json({ status: true, message: 'Content Created Successfully', data: content.insertId })
                        }
                    }
                });

            }
        });


    }
}

//post photo detail
exports.postPhotoByIdDetail = (req, res) => {

    //upload file
    var datetimestamp = Date.now();
    const file = req.files.photo;
    var nameFile = file.name.split('.')[0] + '_' + datetimestamp + '.' + file.name.split('.')[file.name.split('.').length - 1];


    var photoReqData = {
        id_detail: req.body.id_detail,
        category: req.body.category,
        photo: nameFile,
        position: req.body.position,
        created_at: new Date(),
        updated_at: new Date()
    };

    // check null
    if (req.body.contructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({ success: false, message: 'Please fill all field' });
    } else {
        file.mv('uploads/photo_content/' + nameFile, (err) => {
            if (err) {
                console.log(err);
            } else {
                PhotoDetailModel.postPhotoByIdDetail(photoReqData, (err, photos) => {
                    if (err) {
                        res.send(err);
                    } else {
                        if (!photos.affectedRows) {
                            res.send(photos);
                        } else {
                            res.json({ status: true, message: 'Photo Detail Created Successfully', id_data: photos.insertId })
                        }
                    }
                })
            }

        })


    }
}

//check data

