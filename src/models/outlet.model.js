var dbConn = require('../../config/db.config');

var Outlet = function (outlet) {
    this.organization_id = outlet.organization_id,
        this.organization_name = outlet.organization_name,
        this.site = outlet.site,
        this.micro_cluster = outlet.micro_cluster,
        this.cluster = outlet.cluster,
        this.sales_area = outlet.sales_area,
        this.created_at = new Date
}

//get outlet by outlet idOutlet
Outlet.getOutletByIdOutlet = (idOutlet, result) => {
    dbConn.query('SELECT * FROM tb_outlet WHERE organization_id =?', idOutlet, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching outlet by id outlet');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}


module.exports = Outlet;