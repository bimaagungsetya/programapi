var dbConn = require('../../config/db.config');

var JawaraSP = function (jawaraSP) {
    this.organization_id = jawaraSP.organization_id,
        this.act_8gb = jawaraSP.act_8gb,
        this.hit_16gb = jawaraSP.hit_16gb,
        this.act_16gb = jawaraSP.act_16gb,
        this.target = jawaraSP.target,
        this.total_actual = jawaraSP.total_actual,
        this.gap = jawaraSP.gap,
        this.achv = jawaraSP.achv,
        this.partisicipant = jawaraSP.partisicipant,
        this.status = jawaraSP.status,
        this.created_at = new Date
}

//get jawara by idOutlet
JawaraSP.getJawaraSPByIdOutlet = (idOutlet, result) => {
    dbConn.query('SELECT * FROM tb_jawara_sp WHERE organization_id =?', idOutlet, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching jawara sp by id outlet');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}


module.exports = JawaraSP;