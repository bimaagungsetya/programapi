var dbConn = require('../../config/db.config');

var ContactUs = function (contactUs) {
    this.id_contact = contactUs.id_contact,
        this.id_user = contactUs.id_user,
        this.message = contactUs.message,
        this.created_at = new Date
}

//send message
ContactUs.sendMessage = (messageData, result) => {
    dbConn.query('INSERT INTO tb_contact SET ?', messageData, (err, res) => {
        if (err) {
            console.log(err);
            console.log('Error while send message data');
            result(null, err);
        } else {
            console.log('Send message successfully');
            result(null, res);
        }
    })
}

module.exports = ContactUs;