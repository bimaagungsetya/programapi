var dbConn = require('../../config/db.config');

var Content = function (content) {
    this.id_detail = content.id_detail,
        this.title = content.title,
        this.periode = content.periode,
        this.category = content.category,
        this.ketentuan = content.ketentuan,
        this.loyalti = content.loyalti,
        this.info = content.info,
        this.cover = content.cover,
        this.new_content = content.new_content,
        this.created_at = new Date,
        this.updated_at = new Date
}

//get new content by category limit 5
Content.getNewContentCategory = (category, result) => {
    dbConn.query('SELECT * FROM tb_detail WHERE category =? ORDER BY created_at ASC LIMIT 5', category, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching new content by category');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//get content by category
Content.getContentCategory = (category, result) => {
    dbConn.query('SELECT * FROM tb_detail WHERE category =? ORDER BY created_at ASC', category, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching content by category');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//create new content
Content.createContent = (contentData, result) => {
    dbConn.query('INSERT INTO tb_detail SET ?', contentData, (err, res) => {
        if (err) {
            console.log(err);
            console.log('Error while inserting data');
            result(null, err);
        } else {
            console.log('COntent created successfully');
            result(null, res);
        }
    })
}





module.exports = Content;