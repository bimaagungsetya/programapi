var dbConn = require('../../config/db.config');

var Product = function (product) {
    this.id_product = product.id_product,
        this.title = product.title,
        this.sub_title = product.sub_title,
        this.description = product.description,
        this.cover = product.cover,
        this.video = product.video,
        this.contact = product.contact,
        this.count_like = product.count_like,
        this.created_at = new Date,
        this.updated_at = new Date
}

//get new product limit 5
Product.getNewProduct = (result) => {
    dbConn.query('SELECT * FROM tb_product ORDER BY created_at ASC LIMIT 5', (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching new product');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//get product 
Product.getAllProduct = (reqProduct, result) => {
    dbConn.query('SELECT * FROM tb_product ORDER BY created_at ASC', reqProduct, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching product');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//create new product
Product.createProduct = (reqProduct, result) => {
    dbConn.query('INSERT INTO tb_product SET ?', reqProduct, (err, res) => {
        if (err) {
            console.log(err);
            console.log('Error while inserting product');
            result(null, err);
        } else {
            console.log('Product created successfully');
            result(null, res);
        }
    })
}



module.exports = Product;