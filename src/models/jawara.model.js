var dbConn = require('../../config/db.config');

var Jawara = function (jawara) {
    this.id_outlet = jawara.id_outlet,
        this.program = jawara.program,
        this.prize = jawara.prize,
        this.target = jawara.target,
        this.actual_hit = jawara.actual_hit,
        this.actual_amount = jawara.actual_amount,
        this.gap = jawara.gap,
        this.cross_hit = jawara.cross_hit,
        this.cross_amount = jawara.cross_amount,
        this.cross_sell = jawara.cross_sell,
        this.hit_oldsp = jawara.hit_oldsp,
        this.amount_oldsp = jawara.amount_oldsp,
        this.hit_newsp = jawara.hit_newsp,
        this.amount_newsp = jawara.amount_newsp,
        this.hit_vcr = jawara.hit_vcr,
        this.hit_vcr_2 = jawara.hit_vcr_2,
        this.status = jawara.status,
        this.incentive_rate = jawara.incentive_rate,
        this.incentive = jawara.incentive,
        this.participant = jawara.participant,
        this.jumlah_outelt_achv_100K = jawara.jumlah_outelt_achv_100K,
        this.date_updated = jawara.date_updated,
        this.created_at = jawara.created_at
}

//get jawara by idOutlet
Jawara.getJawaraByIdOutlet = (idOutlet, result) => {
    dbConn.query('SELECT * FROM tb_jawara WHERE id_outlet =?', idOutlet, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching jawara by id outlet');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

module.exports = Jawara;