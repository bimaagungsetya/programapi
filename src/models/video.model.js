var dbConn = require('../../config/db.config');

var Video = function (videos) {
    this.id_video = videos.id_video,
        this.title = videos.title,
        this.cover = videos.cover,
        this.video = videos.video,
        this.detail = video.detail,
        this.periode = videos.periode,
        this.new_content = video.new_content,
        this.created_at = new Date,
        this.updated_at = new Date
}

//get new video content 
Video.getNewVideo = (result) => {
    dbConn.query('SELECT * FROM tb_videos ORDER BY created_at ASC LIMIT 5', (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching new video content');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//get all video content 
Video.getAllVideo = (result) => {
    dbConn.query('SELECT * FROM tb_videos ORDER BY created_at ASC', (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching video content');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//post video by id  detail
Video.createVideo = (videoData, result) => {

    dbConn.query('INSERT INTO tb_videos SET ?', videoData, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while insert video content');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

module.exports = Video;
