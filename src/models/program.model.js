var dbConn = require('../../config/db.config');

var Program = function (program) {
    this.id_program = program.id_program,
        this.title = program.title,
        this.sub_title = program.sub_title,
        this.description = program.description,
        this.cover = program.cover,
        this.video = program.video,
        this.contact = program.contact,
        this.count_like = program.count_like,
        this.created_at = new Date,
        this.updated_at = new Date
}

//get new product limit 5
Program.getNewProgram = (result) => {
    dbConn.query('SELECT * FROM tb_program ORDER BY created_at ASC LIMIT 5', (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching new program');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//get product 
Program.getAllProgram = (reqProgram, result) => {
    dbConn.query('SELECT * FROM tb_program ORDER BY created_at ASC', reqProgram, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching program');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//create new product
Program.createProgram = (reqProgram, result) => {
    dbConn.query('INSERT INTO tb_program SET ?', reqProgram, (err, res) => {
        if (err) {
            console.log(err);
            console.log('Error while inserting program');
            result(null, err);
        } else {
            console.log('Program created successfully');
            result(null, res);
        }
    })
}



module.exports = Program;