var dbConn = require('../../config/db.config');

var PhotoDetail = function (photoDetail) {
    this.id_detail_photo = photoDetail.id_detail,
        this.id_detail = photoDetail.id_detail,
        this.category = photoDetail.category,
        this.photo = photoDetail.photo,
        this.position = photoDetail.position,
        this.created_at = new Date,
        this.updated_at = new Date
}

//get photo by id  detail
PhotoDetail.getPhotoByIdDetail = (reqDataPhoto, result) => {
    console.log(reqDataPhoto);
    dbConn.query('SELECT * FROM tb_detail_photos WHERE id_detail =? AND category =? AND position =?', [reqDataPhoto.id_detail, reqDataPhoto.category, reqDataPhoto.position], (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching photo by id detail');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//post photo by id  detail
PhotoDetail.postPhotoByIdDetail = (photoData, result) => {

    dbConn.query('INSERT INTO tb_detail_photos SET ?', photoData, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while insert photo detail content');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

module.exports = PhotoDetail;