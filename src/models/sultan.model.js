var dbConn = require('../../config/db.config');

var Sultan = function (sultan) {
    this.id_outlet = sultan.id_outlet,
        this.program = sultan.program,
        this.prize = sultan.prize,
        this.baseline = sultan.baseline,
        this.actual_hit = sultan.actual_hit,
        this.actual_amount = sultan.actual_amount,
        this.gap = sultan.gap,
        this.cross_hit = sultan.cross_hit,
        this.cross_amount = sultan.cross_amount,
        this.cross_sell = sultan.cross_sell,
        this.hit_oldsp = sultan.hit_oldsp,
        this.amount_oldsp = sultan.amount_oldsp,
        this.hit_newsp = sultan.hit_newsp,
        this.amount_newsp = sultan.amount_newsp,
        this.hit_vcr = sultan.hit_vcr,
        this.hit_vcr_2 = sultan.hit_vcr_2,
        this.status = sultan.status,
        this.incentive_rate = sultan.incentive_rate,
        this.incentive = sultan.incentive,
        this.participant = sultan.participant,
        this.date_updated = sultan.date_updated,
        this.created_at = sultan.created_at
}

//get sultan by idOutlet
Sultan.getSultanByIdOutlet = (idOutlet, result) => {
    dbConn.query('SELECT * FROM tb_sultan WHERE id_outlet =?', idOutlet, (err, res) => {
        if (err) {
            console.log(res);
            console.log('Error while fetching sultan by id outlet');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}


module.exports = Sultan;