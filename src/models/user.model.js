var dbConn = require('../../config/db.config');

var User = function (user) {
    this.id_user = user.id_user,
        this.name = user.name,
        this.username = user.username,
        this.password = user.password,
        //this.avatar = user.avatar,
        this.created_at = new Date,
        this.updated_at = new Date
}

User.getAllUser = (result) => {
    dbConn.query('SELECT * FROM tb_users', (err, res) => {
        if (err) {
            console.log('Error while fetching user');
            result(null, err);
        } else {
            console.log('User while fetching successfully');
            result(null, res);
        }
    })
}

User.getUserByID = (id, result) => {
    dbConn.query('SELECT * FROM tb_users WHERE id_user =?', id, (err, res) => {
        if (err) {
            console.log('Error while fetching user by id');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

//create new user
User.createUser = (userData, result) => {
    dbConn.query('INSERT INTO tb_users SET ?', userData, (err, res) => {
        if (err) {
            console.log(err);
            console.log('Error while inserting data');
            result(null, err);
        } else {
            console.log('User created successfully');
            result(null, res);
        }
    })
}

User.checkUser = (userData, result) => {
    dbConn.query('SELECT * FROM tb_users WHERE username = ? AND password = ?', [userData.username, userData.password], (err, res) => {
        if (err) {
            console.log('Error verification failed');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

User.changePass = (userData, result) => {
    dbConn.query('UPDATE tb_users SET password = ? , updated_at = ? WHERE id_user = ? ', [userData.password, userData.updated_at, userData.id_user], (err, res) => {
        if (err) {
            console.log('Error change password failed');
            result(null, err);
        } else {
            result(null, res);
        }
    })
}

module.exports = User;