const express = require('express');
const router = express.Router();


const contentController = require('../controllers/content.controller');


//get content by category
router.get('/:category', contentController.getContentCategory);

//get new content by category
router.get('/new/:category', contentController.getNewContentCategory);

//get photo by id detail
router.post('/photo/', contentController.getPhotoByIdDetail);


router.post('/create_photo/', contentController.postPhotoByIdDetail);

//create new content
router.post('/', contentController.createContent);

module.exports = router;