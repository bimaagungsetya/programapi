const express = require('express');
const router = express.Router();

const contactUsController = require('../controllers/contact_us.controller');

//send message
router.post('/', contactUsController.sendMessage);

module.exports = router;