const express = require('express');
const router = express.Router();

const userController = require('../controllers/user.controller');

//get all user
router.get('/', userController.getUserList);

//get user by id
router.get('/:id', userController.getUserByID);

//get user by id
router.post('/change_password/', userController.changePass);

module.exports = router;