const express = require('express');
const router = express.Router();

const photoMainController = require('../controllers/photo_main.controller');

//get all photos main
router.get('/', photoMainController.getMainPhoto);

module.exports = router;