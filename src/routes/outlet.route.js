const express = require('express');
const router = express.Router();

const outletController = require('../controllers/outlet.controller');

//get outlet by id outlet
router.get('/:id_outlet', outletController.getOutletByIdOutlet);

module.exports = router;