const express = require('express');
const router = express.Router();


const hppController = require('../controllers/hpp.controller');


//create new content
router.post('/', hppController.getHpp);


module.exports = router;