const express = require('express');
const router = express.Router();

const productController = require('../controllers/product.controller');

//get all content
router.get('/', productController.getAllProduct);

//get new content
router.get('/new/', productController.getNewProduct);

//create content
router.post('/create/', productController.createProduct);

module.exports = router;