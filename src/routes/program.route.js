const express = require('express');
const router = express.Router();

const programController = require('../controllers/program.controller');

//get all program
router.get('/', programController.getAllProgram);

//get new program
router.get('/new/', programController.getNewProgram);

//create program
router.post('/create/', programController.createProgram);

module.exports = router;