const express = require('express');
const router = express.Router();

const achvOutletController = require('../controllers/achv_outlet.controller');

//get jawara by id outlet
router.get('/jawara/:id_outlet', achvOutletController.getJawaraByIdOutlet);

//get jawara sp by id outlet
router.get('/jawara_sp/:id_outlet', achvOutletController.getJawaraSPByIdOutlet);

//get sultan by id outlet
router.get('/sultan/:id_outlet', achvOutletController.getSultanByIdOutlet);

module.exports = router;