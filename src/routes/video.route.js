const express = require('express');
const router = express.Router();

const videoController = require('../controllers/video.controller');

//get new video
router.get('/new/', videoController.getNewVideo);

//get all video
router.get('/', videoController.getAllVideo);

//create new video
router.post('/create/', videoController.createVideo);

module.exports = router;