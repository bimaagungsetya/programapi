const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');

//create express
const app = express();

//setup the server port
const port = process.env.PORT || 3001;

//parse request data content type application/x-www-form-rulencoded
app.use(bodyParser.urlencoded({ extended: true }));

//parse request data content type application/json
app.use(bodyParser.json());

//setup file upload
app.use(fileUpload());



//setup morgan
app.use(morgan('dev'));

//define root route
app.get('/', (req, res) => {
    res.send('Hello Word');
});

//import verification token bearer
const verification = require('./middleware/verification');

//import user route
const userRoutes = require('./src/routes/user.route');

//import content route
const contentRoutes = require('./src/routes/content.route');

//import photo main route
const photoMainRoutes = require('./src/routes/photos_main.route');

//import video route
const videoRoutes = require('./src/routes/video.route');

//import contact route
const contactUsRoutes = require('./src/routes/contact_us.route');

//import outlet route
const outletRoutes = require('./src/routes/outlet.route');

//import achv outlet route
const achvOutletRoutes = require('./src/routes/achv_outlet.route');

//import product route
const productRoutes = require('./src/routes/product.route');

//import program route
const programRoutes = require('./src/routes/program.route');

//import calculator hpp route
const hppRoutes = require('./src/routes/hpp.route');



//user routes
app.use('/api/v1/user', verification(), userRoutes);

//content routes
app.use('/api/v1/content', verification(), contentRoutes);

//photo main routes
app.use('/api/v1/photo_main', verification(), photoMainRoutes);

//video main routes
app.use('/api/v1/video', verification(), videoRoutes);

//contact us routes
app.use('/api/v1/contact_us', verification(), contactUsRoutes);

//outlet routes
app.use('/api/v1/outlet', verification(), outletRoutes);

//outlet routes
app.use('/api/v1/achv_outlet', verification(), achvOutletRoutes);

//product routes
app.use('/api/v1/product', verification(), productRoutes);

//program routes
app.use('/api/v1/program', verification(), programRoutes);

//hpp
app.use('/api/v1/hpp', verification(), hppRoutes);



//route in middleware auth
app.use('/auth', require('./middleware'));

//url file
app.use('/detail_photo', express.static('uploads/photo_content')); //http://localhost:5000/detail_photos/item-2.png

//url file avatar
app.use('/avatar', express.static('uploads/avatars')); //http://localhost:5000/avatar/

//url file photo main
app.use('/main_photo', express.static('uploads/main_photos')); //http://localhost:5000/main_photo/

//url file cover content
app.use('/cover_content', express.static('uploads/cover_content')); //http://localhost:5000/cover_content/

//url file cover content
app.use('/video', express.static('uploads/video')); //http://localhost:5000/cover_content/


//listen to port
app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});